package ru.unn.lesson1;

import java.util.Scanner;

public class Main{
    public static void main(String[] args) {
        Scanner in = new Scanner (System.in);
        System.out.println("Введите первое число");
        float number1 = in.nextFloat();

        System.out.println("Введите знак операции (+, -, *, /, %)");
        String operation = in.next();
        boolean mistake = false;
        if (operation == "/" | operation == "%") {mistake = true};

        System.out.println("Введите второе число");
        float number2 = in.nextFloat();
        if (number2 == 0 && mistake == true) {System.out.println("На ноль делить нельзя");}
        else {

            float result = 0;

            switch (operation) {
                case "+": result = number1 + number2;
                    break;
                case "-": result = number1 - number2;
                    break;
                case "*": result = number1 * number2;
                    break;
                case "/": result = number1 / number2;
                    break;
                case "%": result = number1 % number2;
                    break;}


            System.out.println("Ответ: " + result );}
    }
}